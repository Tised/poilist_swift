//
//  POIListTests.swift
//  POIListTests
//
//  Created by Dmytro Vodnik on 4/21/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import XCTest
@testable import POIList

class POIListTests: XCTestCase {
    
    //MARK: POIList Tests
    
    func testPOIInit() {
        
        //Success case
        let somePoi = POI(name: "some poi", lat: 43.23, lng: 43.26, photo: nil, rating: 4);
        XCTAssertNotNil(somePoi)
        
        //Fail case
        let error = POI(name: "", lat: 43.23, lng: 43.26, photo: nil, rating: 4);
        XCTAssertNil(error)
        
        let badCoords = POI(name: "", lat: 0, lng: 43.26, photo: nil, rating: 4);
        XCTAssertNil(badCoords, "Add coordinates more then 0 please")
    }
    
}
