//
//  ItemPOITableViewCell.swift
//  POIList
//
//  Created by Dmytro Vodnik on 5/4/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import UIKit

class ItemPOITableViewCell: UITableViewCell {
    
    //MARK: properties
    
    @IBOutlet weak var poiImage: UIImageView!
    
    @IBOutlet weak var poiName: UILabel!

    @IBOutlet weak var poiRating: RatingControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
