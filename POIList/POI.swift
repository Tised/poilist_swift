//
//  POI.swift
//  POIList
//
//  Created by Dmytro Vodnik on 4/21/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import UIKit

class POI: NSObject, NSCoding {
    
    //MARK: properties
    
    var name: String
    var lat: Double
    var lng: Double
    var photo: UIImage?
    var rating: Int
    
    // MARK: Archiving Paths
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("pois")

    
    // MARK: Types

    struct PropertyKey {
        
        static let nameKey = "name"
        static let photoKey = "photo"
        static let ratingKey = "rating"
        static let latKey = "lat"
        static let lngKey = "lng"
    }

    
    // MARK: Initialization
    
    init?(name: String, lat: Double, lng: Double, photo: UIImage?, rating: Int) {
        // Initialize stored properties.
        self.name = name
        self.lat = lat
        self.lng = lng
        self.photo = photo
        self.rating = rating
        
        super.init()
        
        if name.isEmpty || (lat == 0 || lng == 0) {
            return nil
        }
    }
    
    //MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(name, forKey: PropertyKey.nameKey)
        aCoder.encodeObject(photo, forKey: PropertyKey.photoKey)
        aCoder.encodeInteger(rating, forKey: PropertyKey.ratingKey)
        aCoder.encodeDouble(lat, forKey: PropertyKey.latKey)
        aCoder.encodeDouble(lng, forKey: PropertyKey.lngKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let name = aDecoder.decodeObjectForKey(PropertyKey.nameKey) as! String
        let photo = aDecoder.decodeObjectForKey(PropertyKey.photoKey) as! UIImage
        let rating = aDecoder.decodeIntegerForKey(PropertyKey.ratingKey)
        let lat = aDecoder.decodeDoubleForKey(PropertyKey.latKey)
        let lng = aDecoder.decodeDoubleForKey(PropertyKey.lngKey)
        
        self.init(name: name, lat: lat, lng: lng, photo: photo, rating: rating);
    }
}
