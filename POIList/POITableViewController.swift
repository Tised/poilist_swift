//
//  POITableViewController.swift
//  POIList
//
//  Created by Dmytro Vodnik on 5/4/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import UIKit

class POITableViewController: UITableViewController {
    
    //MARK: properties
    
    var pois = [POI] ()
    
    func loadPOIData() {
        
        let photo1 = UIImage(named: "kambodj")
        let poi1 = POI(name: "kambodj", lat: 45.23, lng: 32.43, photo: photo1, rating: 4)!
        
        let photo2 = UIImage(named: "tokio")
        let poi2 = POI(name: "tokio", lat: 45.23, lng: 32.43, photo: photo2, rating: 5)!
        
        let photo3 = UIImage(named: "hong kong")
        let poi3 = POI(name: "hong kong", lat: 45.23, lng: 32.43, photo: photo3, rating: 3)!
        
        
        pois += [poi1, poi2, poi3]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = editButtonItem()
        
        if let savedPOIs = loadPOIs() {
            
            pois += savedPOIs
        } else {
        
            loadPOIData()
        }
        self.tableView.rowHeight = 90.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pois.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        // Configure the cell...

        let cellIdentifier = "ItemPOITableViewCell"
        
        let poiItem = pois[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ItemPOITableViewCell
        
        cell.poiName.text = poiItem.name
        cell.poiImage.image = poiItem.photo
        cell.poiRating.rating = poiItem.rating
        
        return cell
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            // Delete the row from the data source
            pois.removeAtIndex(indexPath.row)
            savePOIs()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    //MARK: Actions
    
    @IBAction func unwindToPOIList(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.sourceViewController as? AddPoiController, poi = sourceViewController.poi {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                //update
                pois[selectedIndexPath.row] = poi
                
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            
            } else {
            
                //add new
                let newIndexPath = NSIndexPath(forRow: pois.count, inSection: 0)
                
                pois.append(poi)
                
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Left)
            }
            
            savePOIs()
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowDetail" {
            
            let addPoiController = segue.destinationViewController as! AddPoiController
            
            if let selectedPOICell = sender as? ItemPOITableViewCell {
                
                let indexPath = tableView.indexPathForCell(selectedPOICell)
                let selectedPOI = pois[indexPath!.row]
                
                addPoiController.poi = selectedPOI
            }
        }
        else if segue.identifier == "AddItem" {
            
            print("adding new POI started")
        }
    }
    
    // MARK: NSCoding
    
    func savePOIs() {
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(pois, toFile: POI.ArchiveURL.path!)
        
        if !isSuccessfulSave {
        
            print("Error while save")
        }
    }
    
    func loadPOIs() -> [POI]? {
        
        return NSKeyedUnarchiver.unarchiveObjectWithFile(POI.ArchiveURL.path!) as? [POI]
    }
}
