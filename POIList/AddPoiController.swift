//
//  AddPoiController.swift
//  POIList
//
//  Created by Dmytro Vodnik on 4/21/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import UIKit

class AddPoiController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    
    // MARK: Properties
    @IBOutlet weak var poiImageView: UIImageView!
    @IBOutlet weak var doneAddNewPOI: UIBarButtonItem!
    @IBOutlet weak var poiName: UITextField!
    @IBOutlet weak var ratinControl: RatingControl!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var poi: POI?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        poiName.delegate = self
        
        if let poi = poi {
            
            poiImageView.image = poi.photo
            poiName.text = poi.name
            navigationItem.title = poi.name
            ratinControl.rating = poi.rating
        }
        
        checkValidPOIName()
        print("started add new POI")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func onImageTap(sender: UITapGestureRecognizer) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK: Navigation
    @IBAction func cancelClick(sender: AnyObject) {
        
        let isPresentingInAddPOIMode = presentingViewController is UINavigationController
        
        if isPresentingInAddPOIMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch sender as! UIBarButtonItem {
        
        case doneAddNewPOI:
            print("done clicked")
            let name = poiName.text ?? ""
            let photo = poiImageView.image
            let rating = ratinControl.rating
            
            poi = POI(name: name, lat: 34.56235, lng: 32.6334, photo: photo, rating: rating)
        
        case cancelButton:
            print("cancel clicked")
            
        default:
            print("default here")
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        poiImageView.image = selectedImage
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return false
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        // Disable the Save button while editing.
        doneAddNewPOI.enabled = false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        checkValidPOIName()
        navigationItem.title = textField.text
    }
    
    func checkValidPOIName() {
        
        let text = poiName.text ?? ""
        
        doneAddNewPOI.enabled = !text.isEmpty
    }
    
}
