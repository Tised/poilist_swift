//
//  RatingControl.swift
//  POIList
//
//  Created by Dmytro Vodnik on 4/28/16.
//  Copyright © 2016 EzGood. All rights reserved.
//

import UIKit

class RatingControl: UIView {
    
    // MARK: Properties
    
    var rating = 0 {
        
        didSet {
            setNeedsLayout()
            
            print("changed rating to " + String(rating))
        }
    }
    var ratingButtons = [UIButton]()
    let spacing = 5;
    let starCount = 5;
    
    // MARK: Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let filledStarImage = UIImage(named: "filledStar")
        let emptyStarImage = UIImage(named: "emptyStar")
        
        for i in 0..<starCount {
        
            let button = UIButton()
            button.setTitle("title " + String(i), forState: UIControlState.Normal)
        
            button.setImage(emptyStarImage, forState: .Normal)
            button.setImage(filledStarImage, forState: .Selected)
            button.setImage(filledStarImage, forState: [.Highlighted, .Selected])
            button.adjustsImageWhenHighlighted = false
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(_:)), forControlEvents: .TouchDown)
        
            ratingButtons += [button]
            
            addSubview(button)
        }
    }
    
    override func layoutSubviews() {
        
        let buttonSize = Int(frame.size.height)
        var buttonFrame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        
        for (index, button) in ratingButtons.enumerate() {
            buttonFrame.origin.x = CGFloat(index * (buttonSize + spacing))
            button.frame = buttonFrame
        }
        
        updateButtonsSelectionStates()
    }
    
    override func intrinsicContentSize() -> CGSize {
        let buttonSize = Int(frame.size.height)
        let width = (buttonSize * starCount) + (spacing * (starCount - 1))
        
        return CGSize(width: width, height: buttonSize)
    }
    
    //MARK: Button Action
    func ratingButtonTapped(button: UIButton) {
        
        print("Button " + button.titleLabel!.text! + " pressed")
        
        rating = ratingButtons.indexOf(button)! + 1
        
        print("selected rating = " + String(rating))
        
        updateButtonsSelectionStates()
    }
    
    func updateButtonsSelectionStates() {
    
        for (index, button) in ratingButtons.enumerate() {
            // If the index of a button is less than the rating, that button should be selected.
            button.selected = index < rating
        }
    }

}
